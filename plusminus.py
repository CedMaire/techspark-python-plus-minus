import random

LOWER_BOUND = 0
HIGHER_BOUND = 5000


def generate_random_number():
    return random.randint(LOWER_BOUND, HIGHER_BOUND)


def analyse_input(entered_number, target_number):
    if entered_number > target_number:
        print("Minus")
        return -1
    else:
        print("Plus")
        return 1


def ask_perfect_input(lower, higher):
    return int(input("Your guess (best would be " + str(int((lower + higher) / 2)) + ")): "))


target = generate_random_number()

lowerBound = LOWER_BOUND
higherBound = HIGHER_BOUND

entered = ask_perfect_input(lowerBound, higherBound)
numberOfGuesses = 1

while entered != target:
    biggerOrLower = analyse_input(entered, target)
    if biggerOrLower < 0:
        higherBound = entered
    else:
        lowerBound = entered

    entered = ask_perfect_input(lowerBound, higherBound)
    numberOfGuesses += 1

print("Congrats! You needed " + str(numberOfGuesses) + " guesses.")
